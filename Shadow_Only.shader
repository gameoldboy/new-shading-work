﻿Shader "CH's Shader/Shadow_Only" {
	Properties {
	}
	SubShader {
		Tags {
			"Queue"="Transparent"
			"RenderType"="Opaque"
			}

		Pass {
			Tags { "LightMode"="ForwardBase" }
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma multi_compile_fwdbase_fullshadows	
			#pragma vertex vert
			#pragma fragment frag
			#define SHADOWS_SCREEN
			#define UNITY_NO_SCREENSPACE_SHADOWS
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "AutoLight.cginc"

			struct a2v {
				float4 vertex : POSITION;
			};

			struct v2f {
				float4 pos : SV_POSITION;
				LIGHTING_COORDS(2,3)
			};

			v2f vert(a2v v) {
			 	v2f o;
			 	o.pos = UnityObjectToClipPos(v.vertex);

			 	// Pass shadow coordinates to pixel shader
			 	TRANSFER_VERTEX_TO_FRAGMENT(o);

			 	return o;
			}

			fixed4 frag(v2f i) : COLOR {
				// UNITY_LIGHT_ATTENUATION not only compute attenuation, but also shadow infos
				float attenuation = LIGHT_ATTENUATION(i);

				return fixed4(float3(0.0,0.0,0.0),1-attenuation);
				//return fixed4(float3(1,1,1)*attenuation,1);
			}
			ENDCG
		}
	}
	FallBack "VertexLit"
}