# README #

This README would normally document whatever steps are necessary to get your application up and running.

**ReadMe有待完善。**

---

## ChangeLog:

2017.10.12 0056

1. 修复皮肤shader detail片元输出部分一个bug
2. 增加可以在gamma模式下显示linear效果(gamma to linear)的G2L shader

2017.10.9 0716

1. "NewHairShading PBR AlphaBlend"更新至ver0.4
2. 修复了"NewHairShading PBR AlphaBlend"无法接受阴影的bug。
3. 新增一个只接受投影的透明Shdaer "Shadow Only" ver0.1

2017.10.8 0608

1. 新增模拟皮肤次表面散射PBR Shader "Skin PBR" ver0.1

2017.10.8 0037

1. "NewHairShading PBR AlphaBlend"更新至ver0.3
2. 修复了"NewHairShading PBR AlphaBlend"灯光强度为0的时候shader全黑的bug。
3. 修复了"NewHairShading PBR AlphaBlend"各向异性高光不会随着灯光强度变化的bug。

---

### What is this repository for? ###

* 这是一个**自用shader仓**，方便在公司和家里开发
* 本Shader修改于Shader Forge

### 未完成的目标

* 皮肤的Detail Maps & Detail Mask（强化毛孔质感）
* 提升阴影精度

### How do I get set up? ###

* "NewHairShading PBR AlphaBlend"
    * "PBR Mask"贴图各个通道代表 R:金属度,G:光滑度,B:AO
* "Skin PBR"
    * "Diffuse Intensity"初始值为0.5，为了保证在使用Light wrapping的情况下也能保持和Standard近似的外观表现

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* QQ:23458057
* Other community or team contact